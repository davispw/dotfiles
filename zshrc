source ~/.dotfiles/.antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle pip
antigen bundle command-not-found
antigen bundle history
antigen bundle kubectl
antigen bundle kube-ps1

# Better Vi-style bindings for ZSH!
# https://github.com/jeffreytse/zsh-vi-mode
antigen bundle jeffreytse/zsh-vi-mode
# Disable zsh-vi-mode cursor indicator because it doesn't reset
# when switching between tmux windows.
ZVM_CURSOR_STYLE_ENABLED=false
ZVM_VI_HIGHLIGHT_BACKGROUND=red
ZVM_LINE_INIT_MODE=$ZVM_MODE_INSERT

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

# Load the theme.
# https://github.com/ohmyzsh/ohmyzsh/wiki/Themes#fino-time
antigen theme fino-time

# Tell Antigen that you're done.
antigen apply

# Enable kube-ps1
kubeon

# Customize theme, based on fino-time:
# * fino-time has 'ruby_prompt_info'. I don't use ruby, so replace with kube-ps1
# * ISO datetime format + @epoch
# * Right-align time https://superuser.com/a/737454 https://en.wikipedia.org/wiki/ANSI_escape_code#Control_characters
# * Colorize based on last exit code https://jonasjacek.github.io/colors/
# * Truncate long string as proportion of terminal width
# * Elapsed time of previous command based on https://gist.github.com/knadh/123bca5cfdae8645db750bfb49cb44b0
# * Indicate zsh-vi-mode status (without using the cursor)
function prompt_char {
  case $ZVM_MODE in
    $ZVM_MODE_NORMAL)
    echo '○'
    ;;
    $ZVM_MODE_INSERT)
    echo '◎'
    ;;
    $ZVM_MODE_VISUAL)
    echo '□'
    ;;
    $ZVM_MODE_VISUAL_LINE)
    echo '▣'
    ;;
    $ZVM_MODE_REPLACE)
    echo '▤'
    ;;
  esac
}
function preexec() {
  _cmd_start_time=$(($(gdate +%s%0N)/1000000))
}
function precmd() {
  _prev_cmd_timer_prompt=
  if [ "$_cmd_start_time" ]; then
    local now=$(($(gdate +%s%0N)/1000000))
    local elapsed=$(($now-$_cmd_start_time))
    _prev_cmd_timer_prompt=$(printf "% 12dms" "$elapsed")
    unset _cmd_start_time
  fi
}
PROMPT="
%(?.%F{040}.%F{red})╭─%\$(echo \$(((\$COLUMNS - 23) / 4)))>…>%n%{$reset_color%}%F{239}@%{$reset_color%}%F{033}$(box_name)%{$reset_color%}%F{239}%<<:%{$reset_color%}%{$terminfo[bold]%}%F{226}%\$(echo \$(( (\$COLUMNS - 23) * 3 / 4)))<…<%~%<<%{$reset_color%}
%(?.%F{040}.%F{red})│%{$reset_color%}%\$(echo \$((\$COLUMNS - 17)))>…>\$(git_prompt_info)\$(kube_ps1)%<<
%(?.%F{040}.%F{red})╰─\$(prompt_char)%{$reset_color%} "
RPROMPT=%{$'\e[s\e[2A'%F{239}$'\e[18D'%D{%FT%T}%{$reset_color%}$'\e[u\e[3A\e[13D'%(?.%F{040}.%F{red})\$_prev_cmd_timer_prompt%{$reset_color%}$'\e[u\e[1A\e[13D'%F{234}@%D{%s%.}%{$reset_color%}$'\e[u'%}
# Minimalistic kube-ps1 output
KUBE_PS1_SEPARATOR=
KUBE_PS1_DIVIDER="⎈"
KUBE_PS1_SYMBOL_DEFAULT="⚓"
KUBE_PS1_PREFIX=" "
KUBE_PS1_SUFFIX=
KUBE_PS1_SYMBOL_COLOR=239
KUBE_PS1_CTX_COLOR=magenta
# Refresh prompt every minute - ticking clock!
TMOUT=60
TRAPALRM() { zle reset-prompt }

# Set PATH, HOMEBREW_PREFIX, etc.
for brew in ~/.linuxbrew/bin/brew /home/linuxbrew/.linuxbrew/bin/brew /opt/homebrew/bin/brew /usr/local/bin/brew brew; do
    if command -v "$brew" >/dev/null 2>&1; then
		eval $("$brew" shellenv)
        break
    fi
done

# Configure my custom #autoload functions
FPATH=~/.zfunc:$FPATH

# Configure Shell Completion
# https://docs.brew.sh/Shell-Completion#configuring-completions-in-zsh
if type brew &>/dev/null; then
  FPATH=$(brew --prefix)/share/zsh/site-functions:$FPATH

  autoload -Uz compinit
  compinit
fi

# Enable tab completion of aliases. (Expand completions *after* alias instead of before.)
# https://stackoverflow.com/a/61563247/489363
unset complete_aliases

