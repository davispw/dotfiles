" Type [semicolon instead of colon](https://vim.fandom.com/wiki/Map_semicolon_to_colon)
" to enter command mode--save a shift!
" And double-tap ';' to get the original behavior (repeat last tfTF motion).
nmap ; :
noremap ;; ;

" Do not timeout too quickly for key sequences.  For example, typing `c\\`
" using use vim-easymotion, the default timeout (1s) is too quick to think
" about the next key in the sequence, at least while learning.  But do timeout
" for incomplete keycodes." In particular on MacOS, pressing 'Esc' needs a
" timeout to be interpreted as 'Esc' alone; otherwise it is 'Meta'.
set timeoutlen=5000 " (default=1000)
set ttimeoutlen=100 " (default=100)

" Cut-and-Paste: [use system clipboard](https://stackoverflow.com/a/30691754/489363)
" by default for yank/put commands instead of having to explicitly use the "* register.
set clipboard^=unnamed

" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start

set history=1000 " keep 999 lines of command line history
set ruler        " show the cursor position all the time
set showcmd      " display incomplete commands
set incsearch    " do incremental searching

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for quick macro playback.
" Because how many times have you accidentally typed 'Q' and gotten stuck,
" vs. how many times have you actually used Ex mode?
" https://vim.fandom.com/wiki/Short_mappings_for_common_tasks#Record_into_register_.27q.27.2C_playback_with_.27Q.27
map Q @q

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
syntax on
set hlsearch

" Case-insensitive search
set ignorecase " use /xxx/C to disable with flag
set smartcase " unless search includes a capital letter

" Save VIM session when quitting.
"  '1000  - save 1000 file marks
"  f1     - save global marks
"  <1000  - save 1000 lines from each register
"  :1000  - save 1000 lines of command line history
"  @1000  - save 1000 lines of input line history
"  /1000  - save 1000 lines of search history
"  h      - disable 'hlsearch' highlighting when starting
set viminfo='1000,f1,<1000,:1000,@1000,/1000,h

" Install plugins with vim-plug.
"
" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
" Make sure you use single quotes
" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
" Any valid git URL is allowed
call plug#begin('~/.vim/plugged')
" List of plugins (see README.md#vim)
Plug 'https://github.com/easymotion/vim-easymotion' " Quick use: \\<motion> to use enhanced <motion>
Plug 'https://github.com/tpope/vim-surround' " Quick use: yis( to yank text <s>urrounded by parens
Plug 'https://github.com/terryma/vim-multiple-cursors' " Quick use: /search<CR><Ctrl-N><Ctrl-N>I to insert at the next two matches to 'search'
Plug 'https://github.com/tpope/vim-commentary' " Quick use: gcc
Plug 'https://github.com/vim-scripts/ReplaceWithRegister' " Quick use: gr, grr
Plug 'https://github.com/machakann/vim-highlightedyank' " Quick use: y$
Plug 'https://github.com/tommcdo/vim-exchange' " Quick use: cx<motion> (twice)
Plug 'https://github.com/preservim/nerdtree' " Quick use: :NERDTree
" Initialize plugin system
call plug#end()

" EasyMotion settings
" Allow using regular arrow keys, since on my non-qwerty ErgoDox, 'hjkl' don't line up
map <Plug>(easymotion-prefix)<Down> <Plug>(easymotion-j)
map <Plug>(easymotion-prefix)<Up> <Plug>(easymotion-k)
map <Plug>(easymotion-prefix)<Left> <Plug>(easymotion-h)
map <Plug>(easymotion-prefix)<Right> <Plug>(easymotion-l)

" HJKL for Workman keyboard layout when holding the Option key
" Bind <Esc> in case "Use Option as Meta key" set in MacOS Terminal.app
noremap <Esc>y h
noremap <Esc>n j
noremap <Esc>e k
noremap <Esc>o l
" Or if "Use Option as Meta key" is NOT enabled (AND an "Extended" keyboard
" that outputs Unicode combinations when Option is pressed is NOT used), then
" standard <Meta-*> comboswork.
noremap <M-y> h
noremap <M-n> j
noremap <M-e> k
noremap <M-o> l

" Insert space after the cursor.
nnoremap <Space> i<Space><Esc>
" Insert newline above/below the cursor, without moving the cursor.
" Note, according to https://stackoverflow.com/a/16360104/489363
" it is not possible to map <S-CR> in a terminal,
" and it's a bad idea to map <CR> without some fixes.
nnoremap gO m'O<Esc>`'
nnoremap go m'o<Esc>`'
" Insert newline at the cursor.  Opposite of J.
nnoremap <C-J> m'i<CR><Esc>`'

" Don't clear selection after indent.
" And switch to VISUAL-LINE mode, regardless of current mode.
xnoremap > >gv<C-C>`<V`>
xnoremap < <gv<C-C>`<V`>
" Use <Tab> and <S-Tab> to indent selection as well
xnoremap <Tab> >gv<C-C>`<V`>
xnoremap <S-Tab> <gv<C-C>`<V`>

