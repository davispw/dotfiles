
################################################################################
### Custom settings ############################################################
################################################################################

# Whenever displaying the prompt, write the previous line to disk
# Also, beep after a failed command
export PROMPT_COMMAND='history -a; if [ $? != 0 ]; then printf "\a"; fi'

# Store a lot of history.
export HISTFILESIZE=100000

# Don't put duplicates or lines that begin with a space in the history
export HISTCONTROL="ignoreboth"

# Don't put some basic shell commands in the history
HISTIGNORE="[ 	]*:&:bg:fg:exit"

# Set a fancy color prompt
export PS1='\n\[\e[01;34m\]\u@\h/\l (\j Jobs) #\#\[\e[01;31m\] @ \t\n\[\e[01;31m\]\w\n\[\e[01;34m\]\$> \[\e[00m\]'
export PS2='\[\e[01;31m\]> \[\e[00m\]'

# Put history search query results on the prompt instead of executing immediately.
shopt -s histverify

# Use case-insensitive filename globbing.
# This only affects globs.  See ~/.inputrc for tab-expansion.
shopt -s nocaseglob

# Make bash append rather than overwrite the history on disk.
shopt -s histappend

# Ignore source control files for filename completion.
export FIGNORE=".svn:.o:~:.swp:.bak:.bak2:.bak3"

# User must type ^D twice to exit the shell
export IGNOREEOF=1

# Use VIM as the default text editor
export EDITOR=vim
# [VISUAL vs. EDITOR – what’s the difference?](https://unix.stackexchange.com/a/4861/72208)
export VISUAL=vim

