# Graveyard of Failed Experiments

May work for someone else.

## [YADR](https://github.com/skwp/dotfiles)&mdash;Yet Another Dotfile Repo

In addition to managing your dotfiles, zsh, and vim plugins, YADR automatically
[installs](https://github.com/skwp/dotfiles#whats-included-and-how-to-customize)
a bunch of tools.  (Downside: you don't have to learn to set it all up yourself.)
Opinionated.

Optimized for OS-X.  "Linux/Ubuntu not supported." [Hacks for Ubuntu support](https://dev.to/aspittel/my-terminal-setup-iterm2--zsh--30lm)

## MacPorts

* [Uninstalling MacPorts](https://guide.macports.org/chunked/installing.macports.uninstalling.html)

## [dotbot-brewfile](https://github.com/sobolevn/dotbot-brewfile) plugin (MacOS)

Broke dotbot with all kinds of python errors.

## 1password-cli 

1password-cli (`op` command)
seems to only work with a 1Password Teams account.
