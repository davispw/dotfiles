#!/usr/bin/env bash

# Set PATH, HOMEBREW_PREFIX, etc.
for brew in ~/.linuxbrew/bin/brew /home/linuxbrew/.linuxbrew/bin/brew /opt/homebrew/bin/brew /usr/local/bin/brew brew; do
    if command -v "$brew" >/dev/null 2>&1; then
		eval $("$brew" shellenv)
        break
    fi
done

if [ -d "$HOMEBREW_PREFIX" ]; then
    # Homebrew GNU coreutils - without 'g' prefix
    # https://formulae.brew.sh/formula/coreutils
    if [ -d "$HOMEBREW_PREFIX/opt/coreutils/libexec/gnubin" ]; then
        PATH="$HOMEBREW_PREFIX/opt/coreutils/libexec/gnubin${PATH+:$PATH}"
        MANPATH="$HOMEBREW_PREFIX/opt/coreutils/libexec/gnuman:${MANPATH+:$MANPATH}"
        export PATH MANPATH
    fi

    # [Homebrew](https://docs.brew.sh/Shell-Completion)
    if [ -r "$HOMEBREW_PREFIX/etc/profile.d/bash_completion.sh" ]; then
        . "$HOMEBREW_PREFIX/etc/profile.d/bash_completion.sh"
    else
        for COMPLETION in "$HOMEBREW_PREFIX/etc/bash_completion.d/"*; do
            [ -r "$COMPLETION" ] && . "$COMPLETION"
        done
    fi
fi

