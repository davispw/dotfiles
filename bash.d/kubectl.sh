#!/usr/bin/env bash

# See https://kubernetes.io/docs/tasks/tools/install-kubectl/#optional-kubectl-configurations

if command -v kubectl >/dev/null 2>&1; then
    alias k=kubectl

    if command -v complete >/dev/null 2>&1; then
        source <(kubectl completion bash)
        complete -F __start_kubectl k
    else
        echo "bash completion not enabled" 1>&2
    fi
else
    echo "kubectl not found" 1>&2
fi

