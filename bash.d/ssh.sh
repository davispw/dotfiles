#!/bin/sh

# Based on http://holdenweb.blogspot.com/2007/12/cygwin-ssh-agent-control.html

function ssh-agent-start {
    # By default, use a constant socket location.
    # This environment variable is read by future `ssh-add`.
    if [ -n "$SSH_AUTH_SOCK" ]; then
        # Use existing value if forwarding agent from "ssh -a"
        # See if the forwarded agent is still running and usable.
        echo "Using forwarded ssh-agent: $SSH_AUTH_SOCK"
        ssh-add -l >/dev/null 2>&1
        local result=$?
        if [ "$result" -gt 1 ]; then
            echo "Could not connect to forwarded ssh-agent!"
            # Recurse (once) to use defaults.
            unset -v SSH_AUTH_SOCK
            ssh-agent-start
            return "$?"
        elif [ "$result" -eq 1 ]; then
            # Error code 1 means agent is running but no keys added
            ssh-agent-init
        else
            # Code 0 means agent is running with keys. Done.
            # Print the usable keys.
            ssh-add -l
            return "$?"
        fi
    else
        export SSH_AUTH_SOCK="/tmp/.ssh-agent-socket-$USER"
        ssh-agent-init
    fi
}

function ssh-agent-init {
    # See if an agent is still running and usable.
    ssh-add -l >/dev/null 2>&1
    local result="$?"
    if [ "$result" = 2 ]; then
        # Exit status 2 means couldn't connect to ssh-agent; start one now.
        echo "Starting new ssh-agent..."
        ssh-agent -a "$SSH_AUTH_SOCK" >"/tmp/.ssh-agent-script-$USER"
        if [ "$result" = 1 ]; then
            # A zombie socket is lying around from a previous session,
            # but the agent is dead. Try to 
            echo "Zombie ssh-agent socket, trying again..."
            rm -fv "$SSH_AUTH_SOCK" \
                || (r="$?"; echo "Failed to clean up zombie ssh-agent: $SSH_AUTH_SOCK"; return "$r")
            ssh-agent -a "$SSH_AUTH_SOCK" >"/tmp/.ssh-agent-script-$USER" || return "$?"
        fi
        chmod 600 "/tmp/.ssh-agent-script-$USER"
        . "/tmp/.ssh-agent-script-$USER"
    else
        echo "Found running ssh-agent..."
        . "/tmp/.ssh-agent-script-$USER"
    fi

    # Add the default key, if not already available.
    # TODO: also use ~/.ssh/id_dsa and ~/.ssh/identity
    if [ -f ~/.ssh/id_rsa ] && ! ssh-add -L | grep -q -F -e ~/.ssh/id_rsa; then
        ssh-add
    fi
}

# Make it possible to kill the agent.
function ssh-agent-kill {
    if [ "$SSH_AGENT_PID" -gt 0 ] 2>/dev/null; then
        kill "$SSH_AGENT_PID" \
            && unset -v SSH_AGENT_PID \
            && rm -fv "/tmp/.ssh-agent-script-$USER" \
            && rm -fv "/tmp/.ssh-agent-socket-$USER"
    else
        echo "ssh-agent-kill: no running agent"
    fi
}

ssh-agent-start

