FROM ubuntu:groovy-20210225

# Dependencies for dotbot and homebrew-install 
# https://docs.brew.sh/Homebrew-on-Linux#debian-or-ubuntu
RUN apt-get update && apt-get install -y \
        build-essential \
        git \
        python \
        curl \
        file \
        sudo \
    && rm -rf /var/lib/apt/lists/*

# Homebrew Linux install requires non-root user,
# but some scripts also want passwordless 'sudo' access.
# https://stackoverflow.com/a/48329093/489363
ARG USER=davispw
RUN useradd -ms /bin/bash "$USER" \
    && adduser "$USER" sudo \
    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

WORKDIR /home/$USER/
USER $USER

# Separate command for homebrew install to cache image layer
COPY --chown=$USER:$USER ./.homebrew-install/ /home/$USER/.dotfiles/.homebrew-install/
RUN cd .dotfiles && ./.homebrew-install/install.sh

# Separate command for Brewfile install to cache image layer
COPY --chown=$USER:$USER ./Brewfile* /home/$USER/.dotfiles/
COPY --chown=$USER:$USER ./bash.d/homebrew.sh /home/$USER/.dotfiles/bash.d/
RUN cd .dotfiles && \
    find . -type f && \
    . bash.d/homebrew.sh && \
    OS="$(uname -s)" && \
    { \
      cat Brewfile && \
      if [ -r "Brewfile.$OS" ]; then \
        cat "Brewfile.$OS"; \
      fi; \
    } | brew bundle -v --file -

# Install dotfiles
COPY --chown=$USER:$USER ./ /home/$USER/.dotfiles/
RUN cd .dotfiles && ./install

