/*
  Set any config.h overrides for your specific keymap here.
  See config.h options at https://docs.qmk.fm/#/config_options?id=the-configh-file
*/
#define ORYX_CONFIGURATOR
#undef DEBOUNCE
#define DEBOUNCE 40

#define ONESHOT_TAP_TOGGLE 2

#undef RGBLIGHT_HUE_STEP
#define RGBLIGHT_HUE_STEP 24

#undef RGBLIGHT_BRI_STEP
#define RGBLIGHT_BRI_STEP 20

#undef MOUSEKEY_INTERVAL
#define MOUSEKEY_INTERVAL 30

#undef MOUSEKEY_DELAY
#define MOUSEKEY_DELAY 10

#undef MOUSEKEY_MAX_SPEED
#define MOUSEKEY_MAX_SPEED 5

#undef MOUSEKEY_TIME_TO_MAX
#define MOUSEKEY_TIME_TO_MAX 40

#undef MOUSEKEY_WHEEL_MAX_SPEED
#define MOUSEKEY_WHEEL_MAX_SPEED 1

#undef MOUSEKEY_WHEEL_TIME_TO_MAX
#define MOUSEKEY_WHEEL_TIME_TO_MAX 80

#define FIRMWARE_VERSION u8"B4mLQ/Exdb5"
#define RGB_MATRIX_STARTUP_SPD 60
