#include QMK_KEYBOARD_H
#include "version.h"
#include "keymap_german.h"
#include "keymap_nordic.h"
#include "keymap_french.h"
#include "keymap_spanish.h"
#include "keymap_hungarian.h"
#include "keymap_swedish.h"
#include "keymap_br_abnt2.h"
#include "keymap_canadian_multilingual.h"
#include "keymap_german_ch.h"
#include "keymap_jp.h"
#include "keymap_bepo.h"
#include "keymap_italian.h"
#include "keymap_slovenian.h"
#include "keymap_danish.h"
#include "keymap_norwegian.h"
#include "keymap_portuguese.h"

#define KC_MAC_UNDO LGUI(KC_Z)
#define KC_MAC_CUT LGUI(KC_X)
#define KC_MAC_COPY LGUI(KC_C)
#define KC_MAC_PASTE LGUI(KC_V)
#define KC_PC_UNDO LCTL(KC_Z)
#define KC_PC_CUT LCTL(KC_X)
#define KC_PC_COPY LCTL(KC_C)
#define KC_PC_PASTE LCTL(KC_V)
#define ES_LESS_MAC KC_GRAVE
#define ES_GRTR_MAC LSFT(KC_GRAVE)
#define ES_BSLS_MAC ALGR(KC_6)
#define NO_PIPE_ALT KC_GRAVE
#define NO_BSLS_ALT KC_EQUAL
#define LSA_T(kc) MT(MOD_LSFT | MOD_LALT, kc)
#define BP_NDSH_MAC ALGR(KC_8)

enum custom_keycodes {
  RGB_SLD = EZ_SAFE_RANGE,
  ST_MACRO_0,
  ST_MACRO_1,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [0] = LAYOUT_ergodox_pretty(
    LCTL(KC_BSLASH),KC_F1,          KC_F2,          KC_F3,          KC_F4,          KC_F5,          KC_F6,                                          KC_F12,         KC_F11,         KC_F7,          KC_F8,          KC_F9,          KC_F10,         KC_INSERT,
    KC_GRAVE,       KC_Q,           KC_D,           KC_R,           KC_W,           KC_B,           KC_TAB,                                         KC_BSLASH,      KC_J,           KC_F,           KC_U,           KC_P,           KC_EQUAL,       KC_DELETE,
    KC_MINUS,       KC_A,           KC_S,           KC_H,           KC_T,           KC_G,                                                                           KC_Y,           KC_N,           KC_E,           KC_O,           KC_I,           KC_SCOLON,
    MO(3),          KC_Z,           KC_X,           KC_M,           KC_C,           KC_V,           KC_ESCAPE,                                      KC_BSPACE,      KC_K,           KC_L,           KC_COMMA,       KC_DOT,         KC_SLASH,       KC_QUOTE,
    MO(2),          LT(5,KC_SPACE), KC_LBRACKET,    KC_RBRACKET,    KC_LGUI,                                                                                                        MO(5),          KC_LPRN,        KC_RPRN,        MO(3),          MO(2),
                                                                                                    TT(1),          KC_LGUI,        KC_RALT,        KC_RGUI,
                                                                                                                    KC_LSHIFT,      KC_RCTRL,
                                                                                    KC_LSHIFT,      KC_LCTRL,       KC_LALT,        KC_RSHIFT,      KC_ENTER,       KC_SPACE
  ),
  [1] = LAYOUT_ergodox_pretty(
    KC_PSCREEN,     LED_LEVEL,      KC_BRIGHTNESS_DOWN,KC_BRIGHTNESS_UP,WEBUSB_PAIR,    RESET,          DYN_REC_STOP,                                   DYN_REC_STOP,   KC_NO,          KC_AUDIO_MUTE,  KC_AUDIO_VOL_DOWN,KC_AUDIO_VOL_UP,KC_NO,          RGB_MOD,
    KC_SCROLLLOCK,  RGB_TOG,        RGB_VAD,        RGB_VAI,        KC_NO,          KC_NO,          DYN_REC_START1,                                 DYN_REC_START2, KC_NO,          RGB_SLD,        RGB_SPD,        RGB_SPI,        KC_NO,          KC_NO,
    KC_PAUSE,       TOGGLE_LAYER_COLOR,RGB_HUD,        RGB_HUI,        KC_NO,          KC_NO,                                                                          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,
    TO(3),          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          DYN_MACRO_PLAY1,                                DYN_MACRO_PLAY2,KC_NO,          KC_NO,          ST_MACRO_0,     ST_MACRO_1,     KC_NO,          KC_NO,
    TO(2),          TO(5),          KC_NO,          KC_NO,          OSM(MOD_LGUI),                                                                                                  TO(5),          KC_NO,          KC_NO,          TO(3),          TO(2),
                                                                                                    TO(0),          OSM(MOD_LGUI),  OSM(MOD_RALT),  OSM(MOD_RGUI),
                                                                                                                    OSM(MOD_LSFT),  OSM(MOD_RCTL),
                                                                                    KC_CAPSLOCK,    OSM(MOD_LCTL),  OSM(MOD_LALT),  OSM(MOD_LSFT),  KC_NO,          KC_NO
  ),
  [2] = LAYOUT_ergodox_pretty(
    KC_INSERT,      KC_F10,         KC_F9,          KC_F8,          KC_F7,          KC_F11,         KC_F12,                                         KC_F6,          KC_F5,          KC_F4,          KC_F3,          KC_F2,          KC_F1,          LCTL(KC_BSLASH),
    KC_DELETE,      KC_EQUAL,       KC_P,           KC_U,           KC_F,           KC_J,           KC_BSLASH,                                      KC_TAB,         KC_B,           KC_W,           KC_R,           KC_D,           KC_Q,           KC_GRAVE,
    KC_SCOLON,      KC_I,           KC_O,           KC_E,           KC_N,           KC_Y,                                                                           KC_G,           KC_T,           KC_H,           KC_S,           KC_A,           KC_MINUS,
    KC_QUOTE,       KC_SLASH,       KC_DOT,         KC_COMMA,       KC_L,           KC_K,           KC_BSPACE,                                      KC_ESCAPE,      KC_V,           KC_C,           KC_M,           KC_X,           KC_Z,           KC_TRANSPARENT,
    KC_TRANSPARENT, KC_ENTER,       KC_LPRN,        KC_RPRN,        KC_RGUI,                                                                                                        KC_TRANSPARENT, KC_LPRN,        KC_RPRN,        KC_PSCREEN,     KC_TRANSPARENT,
                                                                                                    TO(0),          KC_RGUI,        KC_LALT,        KC_LGUI,
                                                                                                                    KC_RSHIFT,      KC_LCTRL,
                                                                                    KC_RSHIFT,      KC_RCTRL,       KC_RALT,        KC_LSHIFT,      KC_TRANSPARENT, KC_TRANSPARENT
  ),
  [3] = LAYOUT_ergodox_pretty(
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TILD,        KC_KP_PLUS,     KC_NO,          KC_ASTR,        KC_AMPR,        KC_NO,          KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_NO,          KC_7,           KC_8,           KC_9,           KC_KP_PLUS,     KC_TRANSPARENT,
    KC_UNDS,        KC_NO,          KC_CIRC,        KC_PERC,        KC_DLR,         KC_NO,                                                                          KC_NO,          KC_4,           KC_5,           KC_6,           KC_NO,          KC_TRANSPARENT,
    KC_TRANSPARENT, KC_QUES,        KC_HASH,        KC_AT,          KC_EXLM,        KC_NO,          KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_NO,          KC_1,           KC_2,           KC_3,           KC_SLASH,       KC_TRANSPARENT,
    KC_TRANSPARENT, KC_SPACE,       KC_LCBR,        KC_RCBR,        KC_TRANSPARENT,                                                                                                 KC_0,           KC_COMMA,       KC_DOT,         KC_TRANSPARENT, KC_TRANSPARENT,
                                                                                                    TO(0),          KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
                                                                                                                    MO(4),          KC_TRANSPARENT,
                                                                                    MO(4),          KC_TRANSPARENT, KC_TRANSPARENT, MO(4),          KC_TRANSPARENT, KC_TRANSPARENT
  ),
  [4] = LAYOUT_ergodox_pretty(
    LSFT(KC_INSERT),LSFT(KC_F1),    LSFT(KC_F2),    LSFT(KC_F3),    LSFT(KC_F4),    LSFT(KC_F5),    LSFT(KC_F6),                                    LSFT(KC_F12),   LSFT(KC_F11),   LSFT(KC_F7),    LSFT(KC_F8),    LSFT(KC_F9),    LSFT(KC_F10),   KC_TRANSPARENT,
    KC_TILD,        KC_KP_PLUS,     KC_KP_9,        KC_KP_8,        KC_KP_7,        KC_NO,          LSFT(KC_TAB),                                   KC_PIPE,        KC_NO,          KC_AMPR,        KC_ASTR,        KC_NO,          LSFT(KC_KP_PLUS),LSFT(KC_DELETE),
    KC_UNDS,        KC_NO,          KC_KP_6,        KC_KP_5,        KC_KP_4,        KC_NO,                                                                          KC_NO,          KC_DLR,         KC_PERC,        KC_CIRC,        KC_NO,          KC_COLN,
    KC_DQUO,        KC_QUES,        KC_KP_3,        KC_KP_2,        KC_KP_1,        KC_NO,          LSFT(KC_ESCAPE),                                LSFT(KC_BSPACE),KC_NO,          KC_EXLM,        KC_AT,          KC_HASH,        KC_QUES,        KC_DQUO,
    KC_NO,          LSFT(KC_KP_ENTER),KC_DOT,         KC_COMMA,       KC_KP_0,                                                                                                        KC_TRANSPARENT, KC_LCBR,        KC_RCBR,        KC_KP_ENTER,    KC_NO,
                                                                                                    TO(0),          KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
                                                                                                                    KC_TRANSPARENT, KC_TRANSPARENT,
                                                                                    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT
  ),
  [5] = LAYOUT_ergodox_pretty(
    KC_MS_BTN5,     KC_MS_BTN4,     KC_MS_WH_LEFT,  KC_MS_WH_UP,    KC_MS_WH_DOWN,  KC_MS_WH_RIGHT, KC_MS_BTN3,                                     KC_MS_BTN3,     KC_MS_WH_LEFT,  KC_MS_WH_DOWN,  KC_MS_WH_UP,    KC_MS_WH_RIGHT, KC_MS_BTN4,     KC_MS_BTN5,
    KC_DELETE,      KC_MS_BTN2,     KC_MS_LEFT,     KC_MS_UP,       KC_MS_DOWN,     KC_MS_RIGHT,    KC_MS_BTN1,                                     KC_MS_BTN1,     KC_MS_LEFT,     KC_MS_DOWN,     KC_MS_UP,       KC_MS_RIGHT,    KC_MS_BTN2,     KC_DELETE,
    KC_NO,          KC_MS_ACCEL2,   KC_LEFT,        KC_UP,          KC_DOWN,        KC_RIGHT,                                                                       KC_LEFT,        KC_DOWN,        KC_UP,          KC_RIGHT,       KC_MS_ACCEL2,   KC_NO,
    KC_TRANSPARENT, KC_MS_ACCEL0,   KC_HOME,        KC_PGUP,        KC_PGDOWN,      KC_END,         KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_HOME,        KC_PGDOWN,      KC_PGUP,        KC_END,         KC_MS_ACCEL0,   KC_NO,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_DOT,         KC_COMMA,       KC_TRANSPARENT,                                                                                                 KC_TRANSPARENT, KC_COMMA,       KC_DOT,         KC_TRANSPARENT, KC_TRANSPARENT,
                                                                                                    TO(0),          KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
                                                                                                                    KC_TRANSPARENT, KC_TRANSPARENT,
                                                                                    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT
  ),
};


extern bool g_suspend_state;
extern rgb_config_t rgb_matrix_config;

void keyboard_post_init_user(void) {
  rgb_matrix_enable();
}

const uint8_t PROGMEM ledmap[][DRIVER_LED_TOTAL][3] = {
    [2] = { {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242}, {73,141,242} },

    [3] = { {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255}, {0,199,255} },

    [4] = { {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255}, {212,188,255} },

    [5] = { {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250}, {155,192,250} },

};

void set_layer_color(int layer) {
  for (int i = 0; i < DRIVER_LED_TOTAL; i++) {
    HSV hsv = {
      .h = pgm_read_byte(&ledmap[layer][i][0]),
      .s = pgm_read_byte(&ledmap[layer][i][1]),
      .v = pgm_read_byte(&ledmap[layer][i][2]),
    };
    if (!hsv.h && !hsv.s && !hsv.v) {
        rgb_matrix_set_color( i, 0, 0, 0 );
    } else {
        RGB rgb = hsv_to_rgb( hsv );
        float f = (float)rgb_matrix_config.hsv.v / UINT8_MAX;
        rgb_matrix_set_color( i, f * rgb.r, f * rgb.g, f * rgb.b );
    }
  }
}

void rgb_matrix_indicators_user(void) {
  if (g_suspend_state || keyboard_config.disable_layer_led) { return; }
  switch (biton32(layer_state)) {
    case 2:
      set_layer_color(2);
      break;
    case 3:
      set_layer_color(3);
      break;
    case 4:
      set_layer_color(4);
      break;
    case 5:
      set_layer_color(5);
      break;
   default:
    if (rgb_matrix_get_flags() == LED_FLAG_NONE)
      rgb_matrix_set_color_all(0, 0, 0);
    break;
  }
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case ST_MACRO_0:
    if (record->event.pressed) {
      SEND_STRING(SS_TAP(X_DOWN) SS_DELAY(100) SS_TAP(X_HOME) SS_DELAY(100) SS_TAP(X_DOT));

    }
    break;
    case ST_MACRO_1:
    if (record->event.pressed) {
      SEND_STRING(SS_TAP(X_DOWN) SS_DELAY(100) SS_TAP(X_LEFT) SS_DELAY(100) SS_TAP(X_DOT));

    }
    break;
    case RGB_SLD:
      if (record->event.pressed) {
        rgblight_mode(1);
      }
      return false;
  }
  return true;
}

uint32_t layer_state_set_user(uint32_t state) {

  uint8_t layer = biton32(state);

  ergodox_board_led_off();
  ergodox_right_led_1_off();
  ergodox_right_led_2_off();
  ergodox_right_led_3_off();
  switch (layer) {
    case 1:
      ergodox_right_led_1_on();
      break;
    case 2:
      ergodox_right_led_2_on();
      break;
    case 3:
      ergodox_right_led_3_on();
      break;
    case 4:
      ergodox_right_led_1_on();
      ergodox_right_led_2_on();
      break;
    case 5:
      ergodox_right_led_1_on();
      ergodox_right_led_3_on();
      break;
    case 6:
      ergodox_right_led_2_on();
      ergodox_right_led_3_on();
      break;
    case 7:
      ergodox_right_led_1_on();
      ergodox_right_led_2_on();
      ergodox_right_led_3_on();
      break;
    default:
      break;
  }
  return state;
};
