# Source bashrc if it exists.
if [ -e ~/.bashrc ] ; then
  source ~/.bashrc
fi

# Custom PATH.
export PATH=~/.bin:"$PATH"

# Custom scripts.
for file in ~/.bash.d/*.sh; do
    if [ -x "$file" ]; then
        source "$file"
    fi
done

# Enable color "ls" output.
if command -v dircolors >/dev/null 2>&1; then
    eval "$(dircolors -b)"
fi
if ls --color=auto >/dev/null 2>&1; then
	# GNU ls
	alias ls='ls --color=auto'
else
	# MacOS / FreeBSD
	export CLICOLOR=1
fi

# Enable color "grep" output, if supported.
# http://www.ccs.neu.edu/home/katz/unix-colors.html
if echo test | grep --color=auto >/dev/null 2>&1; then
    export GREP_OPTIONS='--color=auto'
fi

