# dotfiles

My dotfiles repo.

> Peter Davis \<davispw@gmail.com>

# Toolset

## [dotbot](https://github.com/anishathalye/dotbot)

## vim

Plugins:

- Managed with [vim-plug](https://github.com/junegunn/vim-plug)

- [vim-easymotion](https://github.com/easymotion/vim-easymotion) - Quick use: `\\<motion>` to use enhanced *<motion>*
- [vim-surround](https://github.com/tpope/vim-surround) - Quick use: `cis'"` to change surrounding `'` to `"`; `v<motion>S'` wraps the selection with `'`
- [vim-multiple-cursors](https://github.com/terryma/vim-multiple-cursors) - Quick use: 
- [commentary](https://github.com/tpope/vim-commentary)
Plug 'https://github.com/easymotion/vim-easymotion' " 
Plug 'https://github.com/tpope/vim-surround' " Quick use: yis( to yank text <s>urrounded by parens
Plug 'https://github.com/terryma/vim-multiple-cursors' " Quick use: /search<CR><Ctrl-N><Ctrl-N>I to insert at the next two matches to 'search'
Plug 'https://github.com/trope/vim-commentary' " Quick use: gcc
Plug 'https://github.com/vim-scripts/ReplaceWithRegister' " Quick use: gr, grr
Plug 'https://github.com/machakann/vim-highlightedyank' " Quick use: y$
Plug 'https://github.com/tommcdo/vim-exchange' " Quick use: cx<motion> (twice)

Settings:
- `;` instead of `:`
- `,` leader
- cut-and-paste from system clipboard

## tmux

[.tmux.conf](./tmux.conf)

* `Ctrl-\` prefix (bound to a Macro key on my ErgoDox-EZ)
  - tmux doesn't support modifier keys other than Ctrl and "Meta",
    but [Meta is `Esc` on MacOS](https://osxdaily.com/2013/02/01/use-option-as-meta-key-in-mac-os-x-terminal/)
    which doesn't work on Windows, so using Meta is difficult.
* [kube-tmux](https://github.com/jonmosco/kube-tmux)
* Set `tmux new-session -t main` as login shell to allow [multiple sessions sharing a single window group](https://gist.github.com/chakrit/5004006)
* Cut-and-Paste to system clipboard on [MacOS](https://github.com/ChrisJohnsen/tmux-MacOSX-pasteboard) and Windows
* vim keybindings for copy-mode (with customizations to more closely match real vim)

## ssh

* Custom [ssh-agent](./bash/ssh) script&mdash;start shared ssh-agent on first login

## git

## mvn

## kubectl

* [Enabling kubectl autocompletion](https://kubernetes.io/docs/tasks/tools/install-kubectl/#enabling-shell-autocompletion) in `zsh` and `bash`

## [matterhorn](https://github.com/matterhorn-chat/matterhorn)&mdash;Mattermost terminal client

## IntelliJ

* IdeaVim

## Mintty + [WSLtty](https://github.com/mintty/wsltty) (Windows)

## Homebrew (package manager)

### [Brewfile](https://github.com/Homebrew/homebrew-bundle)

Installs all of the tools above, plus:
- GNU utilities (replacements for BSD-style commands on MacOS)
- bash-completion
- git
- watch
- wget
- curl
- jq
- yq
- python3
- ruby
- helm

### [cask](https://github.com/Homebrew/homebrew-cask) - Mac Apps

- AutoDesk Fusion 360
- [Docker for Mac](https://codingbee.net/docker/install-docker-for-mac-using-homebrew)

### [mas](https://github.com/mas-cli/mas) - Mac App Store CLI

## 1Password


# Resources

## dotfiles management

* [dotfiles&mdash;Your unofficial guide to dotfiles on ~~GitHub~~*git*](https://dotfiles.github.io/utilities/)

### Examples / Inspiration

* GitHub: [anishathalye / dotfiles](https://github.com/anishathalye/dotfiles)
* Antigen: [typical .zshrc](https://github.com/zsh-users/antigen#usage) with oh-my-zsh! plugins

## `zsh`

* [BASH vs ZSH Linux Shell Tutorial](https://www.youtube.com/watch?v=_DiEbmg3lU8)&mdash;YouTube
* [Apple replaces bash with zsh as the default shell in macOS Catalina](https://www.theverge.com/2019/6/4/18651872/apple-macos-catalina-zsh-bash-shell-replacement-features)

## Vim

* [What is the difference between the vim plugin managers?](https://vi.stackexchange.com/questions/388/what-is-the-difference-between-the-vim-plugin-managers)&mdash;Vi Stack Exchange
* IntelliJ compatibility: [Vim plugins emulated by IdeaVim](https://github.com/JetBrains/ideavim#emulated-vim-plugins) *(This is a good starting list, because IdeaVim maintainers have decided these were worth emulating!)*
* Blog: [Vincent Driessen: How I boosted my Vim](https://nvie.com/posts/how-i-boosted-my-vim/)
* Vim Cheat Sheet
  ![Vim Cheat Sheet](https://rumorscity.com/wp-content/uploads/2014/08/10-Best-VIM-Cheat-Sheet-02.jpg)
* https://engagor.github.io/blog/2018/02/21/why-vim-doesnt-need-multiple-cursors/

## IntelliJ

* [Sharing your IDE settings](https://www.jetbrains.com/help/idea/sharing-your-ide-settings.html)
  - "Settings Sync" to JetBrains account
  - or, "settings repository" (git)

## `tmux`

* YouTube: [Learn Linux TV - Getting Started with tmux Part 1 - Overview and Features](https://www.youtube.com/watch?v=gmjyMxezIWU)
* Book (free): [The Tao of tmux](https://leanpub.com/the-tao-of-tmux/read)
* Book ($18.95): [tmux 2](https://pragprog.com/book/bhtmux2/tmux-2)
* Inspiration: [gpakosz/.tmux](https://github.com/gpakosz/.tmux) on GitHub&mdash;rich example .tmux.conf
* Blog: [Badass Terminal: FCU WSL Edition (oh-my-zsh, powerlevel9k, tmux, and more!)](http://jessicadeen.com/tech/microsoft/badass-terminal-fcu-wsl-edition-oh-my-zsh-powerlevel9k-tmux-and-more/)

## Windows Terminal Emulators

* [ConEmu vs Hyper vs Terminus vs MobaXTerm Terminator vs Ubuntu WSL](https://nickjanetakis.com/blog/conemu-vs-hyper-vs-terminus-vs-mobaxterm-terminator-vs-ubuntu-wsl) (Conclusion: author now uses `wsltty`)

## Homebrew (MacOS)

* [Automate setting up macOS](http://danielsaidi.com/blog/2018/08/26/automate-setting-up-mac-os)

# Ergodox EZ

* [My Layout](https://configure.ergodox-ez.com/ergodox-ez/layouts/B4mLQ/latest/0) (ever-evolving)
  - [Source as of March 30, 2019)(./ergodox/ergodox_ez_davispw-workmanp-one-handed-v3_B4mLQ_4DYJ0/README.md)


