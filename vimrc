" Most of this I collected from a vimrc template a decade or two ago.
" So I don't have the immediate source.  Googling, I see this has been
" plagiarized all over the internet now.  Some of it appears to have
" come from [Bram Moolenaar's own .vimrc](http://cs.jhu.edu/~heshanli/.vimrc)
" (the creator of Vim).

" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" Common settings - vim + IdeaVim
runtime .vim/common.vim

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent        " always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
         \ | wincmd p | diffthis

" Keep backup and swap files in a common location under ~/.vim.
" (Don't store in '.' except as a last resort.)
set backup      
set backupdir-=.
set backupdir+=.
set backupdir^=~/.vim/backup
set directory-=.
set directory+=.
set directory^=~/.vim/swap

" Default tab settings: don't use <Tab> characters.
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

" Wildcard matching in the command-line editor.
" Press <Tab> to match filenames similar to the way Bash does.
set wildchar=<Tab>
set wildmenu " only triggered by 'full' wildmode
set wildmode=longest:full " fill in the longest common substring and open menu
set wildignore+=*.o,*.obj,.svn,*~,.*.swp,*.bak,*.bak2,*.bak3,CVS,.git
set wildignorecase

" Use UTF-8 encoding by default.
" But the terminal encoding needs to stay unchanged from the default.
let &termencoding = &encoding
set encoding=utf-8

" Mouse mode.
" Especially useful in tmux to scroll vim instead of history buffer.
set mouse=a
" Reduce scroll speed
map <ScrollWheelUp> <C-Y>
map <S-ScrollWheelUp> <C-U>
map <ScrollWheelDown> <C-E>
map <S-ScrollWheelDown> <C-D>

source ~/.vim/common.vim

