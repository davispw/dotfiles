#!/bin/sh


defaults write com.apple.HIToolboxApple CurrentKeyboardLayoutInputSourceID "org.sil.ukelele.keyboardlayout.workman.workmanextended";

# Enable tabbing between all controls
defaults write -g AppleKeyboardUIMode -int 2

defaults write com.apple.HIToolboxApple AppleEnabledInputSources -array
defaults write com.apple.HIToolboxApple AppleEnabledInputSources -array-add '
    {
        "Bundle ID" = "com.apple.CharacterPaletteIM";
        InputSourceKind = "Non Keyboard Input Method";
    }'
defaults write com.apple.HIToolboxApple AppleEnabledInputSources -array-add '
    {
        "Bundle ID" = "com.apple.inputmethod.EmojiFunctionRowItem";
        InputSourceKind = "Non Keyboard Input Method";
    }'
defaults write com.apple.HIToolboxApple AppleEnabledInputSources -array-add '
    {
        InputSourceKind = "Keyboard Layout";
        "KeyboardLayout ID" = 16380;
        "KeyboardLayout Name" = "Workman Extended";
    }'
defaults write com.apple.HIToolboxApple AppleEnabledInputSources -array-add '
    {
        InputSourceKind = "Keyboard Layout";
        "KeyboardLayout ID" = 0;
        "KeyboardLayout Name" = "U.S.";
    }'
defaults write com.apple.HIToolboxApple AppleEnabledInputSources -array-add '
    {
        InputSourceKind = "Keyboard Layout";
        "KeyboardLayout ID" = 16382;
        "KeyboardLayout Name" = "Workman-P";
    }'
defaults write com.apple.HIToolboxApple AppleEnabledInputSources -array-add '
    {
        InputSourceKind = "Keyboard Layout";
        "KeyboardLayout ID" = "-20481";
        "KeyboardLayout Name" = "Devanagari-QWERTY";
    }'

# Increase repeat rate
defaults write -g InitialKeyRepeat -int 15 # normal minimum is 15 (225 ms)
defaults write -g KeyRepeat -int 3 # normal minimum is 2 (30 ms)

# Disable èéêëēėę popup when holding keys.  KeyRepeat enabled in all applications.
# Especially IntelliJ with hjkl.
# Logout/Login required after changing.
defaults write -g ApplePressAndHoldEnabled -bool false

# Disable automatic caps, period, quotes, and dashes substitution as it’s annoying when typing code
defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false
defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false
defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

# Disable automatic spelling correction...I know how to type, thanks
defaults write -g NSAutomaticSpellingCorrectionEnabled -bool false
defaults write -g WebAutomaticSpellingCorrectionEnabled -bool false

