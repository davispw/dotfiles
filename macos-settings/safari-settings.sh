#!/bin/sh

# Enable Develop and Debug menus
defaults write com.apple.Safari WebKitDeveloperExtrasEnabledPreferenceKey 1
defaults write com.apple.Safari "WebKitPreferences.developerExtrasEnabled" 1
defaults write com.apple.Safari IncludeDevelopMenu -bool true
defaults write com.apple.Safari IncludeInternalDebugMenu -bool true

# These two settings combine to select:
# General > Safari opens with: All non-private windows from last session
defaults write com.apple.Safari AlwaysRestoreSessionAtLaunch 1
defaults write com.apple.Safari ExcludePrivateWindowWhenRestoringSessionAtLaunch 1

