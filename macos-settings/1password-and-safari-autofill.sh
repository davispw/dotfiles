#!/bin/sh

# https://discussions.apple.com/thread/7084431

# Disable Safari auto-fill so 1Password is used exclusively.
defaults write com.apple.Safari AutoFillPasswords -bool false
