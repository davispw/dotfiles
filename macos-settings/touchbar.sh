#!/bin/sh

# Customize up to 4 buttons when collapsed ("mini").
# List of options: https://blog.eriknicolasgomez.com/2016/11/28/managing-or-setting-the-mini-touchbar-control-strip/

# Volume should be first, or else not enough room to tap-and-drag 
defaults write com.apple.controlstrip MiniCustomized '(
    com.apple.system.volume,
    com.apple.system.brightness,
    com.apple.system.screencapture,
    com.apple.system.siri
)'

# Full control bar:

defaults write com.apple.controlstrip FullCustomized '(
    com.apple.system.input-menu,
    com.apple.system.dictation,
    com.apple.system.group.keyboard-brightness,
    com.apple.system.search,
    com.apple.system.group.media,
    com.apple.system.volume,
    com.apple.system.brightness,
    com.apple.system.screencapture,
    com.apple.system.siri
)'

# Display F1-F12 keys in some applications
#defaults write com.apple.touchbar.agent PresentationModePerApp -dict \
    #"com.apple.Terminal" functionKeys

# Load changed settings.  Will be instantly restarted.
killall ControlStrip

